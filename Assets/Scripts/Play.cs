﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Play : MonoBehaviour{
    //Declaro un metodo para invocar el script
    public void StartGame(){
        //Utilizo metodo para cargar la escena del juego
        SceneManager.LoadScene("Level_02");
    }
   
}
