﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtaqueEnemigo : MonoBehaviour{
    public PlayerControler player;
    void OnTriggerEnter(Collider objeto){
        if(objeto.tag == "Player"){
            player.QuitarVida(10);
        }
    }
    
}
