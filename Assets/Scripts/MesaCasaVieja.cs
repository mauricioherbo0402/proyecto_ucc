﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MesaCasaVieja : MonoBehaviour{
    public Text mensaje;
    public GameObject bloqueador, bloqueador2;
    
    
    void OnTriggerEnter(Collider objeto){
        if (objeto.tag == "Player"){
            //Las ventajas de las corrutinas es que puedo poner
            //pequeños temporizadores para controlar los momentos
            //de las secuencias de eventos
            StartCoroutine("MesaEncontrada");
        }
            
    }

    public IEnumerator MesaEncontrada(){
        mensaje.text = "Ups!! \n Este no es el antidoto sal y revisa la parte trasera";
        yield return new WaitForSeconds(2.0f);
        mensaje.text = "";
        Destroy(bloqueador, 1.0f);
        Destroy(bloqueador2, 1.0f);
    }
}
