﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour{
    // referencia al controlador de animaciones
    private Animator animator;
    // determinamos si esta avanzando o esta quieto
    private bool avanzando;

    // Variables relacionadas con el movimiento del personaje
    public float velocidad  = 3.0f;
    // para las funciones relacionadas con fisicas
    // debo garantizar el acceso al rigidbody del personaje
    private Rigidbody rb;
    // definicion de un vector direccion de movimiento
    Vector3 direccion;

    //nivel de vida
    public int vida = 100;
    public Slider sliderVida;


    public Text mensajes;


    void Start(){
       // es recomendable en este metodo configurara la inicializacion
       // de nuestras variables de objetos de juego

       // accedemos al rigid body
       rb = GetComponent<Rigidbody>();
       // acceder al componente de animaciones"Ejecute x animacion"
       animator = GetComponent<Animator>();

    }

    public void QuitarVida(int perdidaVida){
        vida = vida - perdidaVida;
        Debug.Log("Nivel de vida >>> "+ vida);
    }

    public void ActualizarVida(){
        sliderVida.value = (float)vida;
    }

    public IEnumerator Morir(){
        mensajes.text = "Game over!";
        yield return new WaitForSeconds(3.0f);
        SceneManager.LoadScene("MenuPrincipal");

    }

    // se ejecuta cada frame SIN VALIDACIONES
    // no se recomienda programar los comportamientos fisicos    
    void Update(){
        ActualizarVida();
        if(vida <= 0){
            StartCoroutine("Morir");
        }
        
    }

    // se ejecuta en cada frame PERO VALIDA SU INFORMACION
    // este es metodo ideal para programar eventos relacionados
    // con las fisicas del juego
    void FixedUpdate(){

        //CUANDO REQUIERO UNA CCION ESPECIFICA
        //SOLO DEBO CONFIGURAR CON QUE INPUT DISPARO EL EVENTO
        //EN EL ANIMATOR CONTROLLER

        //Posicion
        if (Input.GetButton("Fire2")){
            //Debug.Log("posicion");
            animator.SetTrigger("posicion");            
        }
        //Gancho arriba
       /*  if (Input.GetButton("Fire1")){
            //Debug.Log("gancho arriba");
            animator.SetTrigger("gancho_arriba");            
        }  */

        //Gancho arriba
        if (Input.GetKeyDown(KeyCode.Q)){
            //Debug.Log("gancho arriba");
            animator.SetTrigger("gancho_arriba");            
        } 

        //Gancho derecho
         if (Input.GetButton("Fire1")){
            //Debug.Log("gancho derecho");
            animator.SetTrigger("gancho_derecho");            
        }
        //Correr
        if (Input.GetKeyDown(KeyCode.LeftShift)){
            //Debug.Log("corriendo");
            animator.SetTrigger("correr");            
        } 

        //Caminar
        /* if (Input.GetKeyDown(KeyCode.W)){
            //Debug.Log("Caminar");
            animator.SetTrigger("walk");            
        }  */

        // identifico las variables de entrada de usuario
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Movimiento(horizontal, vertical);
        // EJECUTO LA ANIMACION CORRESPONDIENTE AL MOVIMIENTO -> CAMINAR
        Animacion(horizontal, vertical);



        
    }

    public void Animacion(float horizontal, float vertical){
        //Debug.Log("Horizontal >>>" + horizontal + " / Vertical >>> "+ vertical);
        avanzando = horizontal != 0f || vertical!= 0f;
        //Debug.Log("Avanzando >>>" + avanzando);
        animator.SetBool("walk", avanzando);
    }

    public void Movimiento(float horizontal, float vertical){
        // se define todo lo relacionado con la direccion hacia la 
        // cual se mueve el personaje
        direccion.Set(horizontal, 0f, vertical);
        direccion = direccion.normalized * velocidad * Time.deltaTime;
        // validar el rigidbody asignado al personaje
        if (rb){
            if (horizontal > 0.0f){
                rb.transform.Translate(
                    Vector3.right * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (horizontal < 0.0f){
                rb.transform.Translate(
                    Vector3.left * velocidad * 0.5f * Time.deltaTime
                );
            }
            if (vertical > 0.0f){
                rb.transform.Translate(
                    Vector3.forward * velocidad * Time.deltaTime
                );
            }
            if (vertical < 0.0f){
                rb.transform.Translate(
                    Vector3.back * velocidad * 0.5f * Time.deltaTime
                );
            }
            
        }

        

    }
}
