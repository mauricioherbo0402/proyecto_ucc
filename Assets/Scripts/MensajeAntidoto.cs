﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MensajeAntidoto : MonoBehaviour{
    public Text mensaje;

    void OnTriggerEnter(Collider objeto){
        if (objeto.tag == "Player"){
            mensaje.text = "Has encontrado la cura \n Ve y salva el planeta";
                       
        }
    }
  
}
