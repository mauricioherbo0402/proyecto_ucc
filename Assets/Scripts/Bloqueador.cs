﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bloqueador : MonoBehaviour{
    public Text mensajes;

    void OnTriggerEnter(Collider objeto){
        if (objeto.tag == "Player"){
            mensajes.text = "Entra a la casa y busca el antidoto";
                       
        }
    }

    void OnTriggerExit(Collider objeto){
        if (objeto.tag == "Player"){
            mensajes.text = "";
                       
        }
    }
   
}
